// Dependencies
import React, { memo } from 'react';
import { Box } from '@chakra-ui/react';
import { ComposableMap } from 'react-simple-maps';

// Components
import { MapItem } from 'components';

interface Props {
  setSelectedProvince: (_: string) => void;
}

const Map: React.FC<Props> = ({ setSelectedProvince }): JSX.Element => {
  return (
    <Box
      align="center"
      m="auto"
      maxW={['70%', 300, null, 320, 310, 400]}
      mb={{ base: 4, sm: 0 }}
      minW={['70%', 300, null, 320, 310, 400]}
      mt={-3}
      pb={2}
    >
      <ComposableMap
        height={1730}
        projection="geoMercator"
        projectionConfig={{ scale: 2200, center: [-64, -40] }}
        style={{ position: 'sticky', top: 24 }}
      >
        <MapItem setSelectedProvince={setSelectedProvince} />
      </ComposableMap>
    </Box>
  );
};

export default memo(Map);
