// Dependencies
import React, { useContext } from 'react';
import { Geographies, Geography } from 'react-simple-maps';

// Utils
import { SelectionContext } from 'utils/Context';

const geoUrl = '/provincia.json';

interface Props {
  setSelectedProvince: (_: string) => void;
}

const MapItem: React.FC<Props> = ({ setSelectedProvince }): JSX.Element => {
  const selectedProvince = useContext(SelectionContext);

  return (
    <>
      <rect
        fill="transparent"
        height="100%"
        width="100%"
        onClick={() => setSelectedProvince('Argentina')}
      />
      <Geographies geography={geoUrl}>
        {({ geographies }) =>
          geographies.map(geo => {
            const displayProvince =
              geo.properties.name === 'Ciudad de Buenos Aires' ? 'CABA' : geo.properties.name;
            const isSelected = displayProvince === selectedProvince ? true : false;
            return (
              <Geography
                key={geo.rsmKey}
                fill={isSelected ? '#8cb4f5' : '#EAEAEC'}
                geography={geo}
                stroke="#555"
                style={{
                  default: { outline: 'none' },
                  hover: { fill: isSelected ? '#5d91e3' : '#DADADC', outline: 'none' },
                  pressed: { fill: isSelected ? '#8cb4f5' : '#DADADC', outline: 'none' },
                }}
                onClick={() => {
                  setSelectedProvince(isSelected ? 'Argentina' : displayProvince);
                }}
              />
            );
          })
        }
      </Geographies>
    </>
  );
};

export default MapItem;
