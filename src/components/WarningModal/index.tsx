// Dependencies
import React from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Flex,
} from '@chakra-ui/react';

interface Props {
  isOpen: boolean;
  handleClose: () => void;
}

const WarningModal: React.FC<Props> = (props): JSX.Element => {
  return (
    <Modal isOpen={props.isOpen} size="2xl" onClose={props.handleClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Aviso importante</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Flex flexDir="column" gridGap={4} mb={6}>
            <p>
              Esta página ya no está siendo actualizada. Se mantiene activa para fines de consulta
              histórica, pero no se puede garantizar la autenticidad de los datos a partir del 18 de
              enero de 2022, y se recomienda no utilizar estos datos sin antes chequear las fuentes.
            </p>
            <p>
              Además, al depender de estos mismos datos, tampoco se puede garantizar el correcto
              funcionamiento del sitio de acá en adelante. Por ejemplo, es posible que el sitio de a
              entender que hay más personas con una dosis de refuerzo que personas con vacunación
              parcial, lo cual no tiene sentido lógico.
            </p>
            <p>Por favor tener esto en cuenta al utilizarlo.</p>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default WarningModal;
